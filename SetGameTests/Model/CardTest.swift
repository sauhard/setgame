//
//  Card.swift
//  SetGameTests
//
//  Created by Sauhard Sahi on 11/3/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import XCTest
@testable import SetGame

class CardTest: XCTestCase {

    func testFeatureCount() {
        XCTAssertEqual(4, Card.numFeatures())
    }
    
    func testInit() {
        let card = Card(withNumber: Card.Feature.OptA,
                        andSymbol: Card.Feature.OptB,
                        andShading: Card.Feature.OptC,
                        andColor: Card.Feature.OptB)
        
        XCTAssertEqual(Card.Feature.OptA, card.number)
        XCTAssertEqual(Card.Feature.OptB, card.shape)
        XCTAssertEqual(Card.Feature.OptC, card.shading)
        XCTAssertEqual(Card.Feature.OptB, card.color)
    }
    
    func testHash() {
        
        let cards: [Card] = [
            Card(withNumber: Card.Feature.OptA,
                            andSymbol: Card.Feature.OptA,
                            andShading: Card.Feature.OptA,
                            andColor: Card.Feature.OptA),
        
            Card(withNumber: Card.Feature.OptA,
                            andSymbol: Card.Feature.OptA,
                            andShading: Card.Feature.OptB,
                            andColor: Card.Feature.OptA),
        
            Card(withNumber: Card.Feature.OptA,
                             andSymbol: Card.Feature.OptC,
                            andShading: Card.Feature.OptB,
                            andColor: Card.Feature.OptA),
        
            Card(withNumber: Card.Feature.OptC,
                             andSymbol: Card.Feature.OptC,
                             andShading: Card.Feature.OptC,
                             andColor: Card.Feature.OptA),
            
            Card(withNumber: Card.Feature.OptA,
                             andSymbol: Card.Feature.OptA,
                             andShading: Card.Feature.OptA,
                             andColor: Card.Feature.OptC),
            
            Card(withNumber: Card.Feature.OptC,
                             andSymbol: Card.Feature.OptC,
                             andShading: Card.Feature.OptC,
                             andColor: Card.Feature.OptC),
        ]
        
        // Every card's hash should be unique.
        for idxFirst in 0..<cards.count {
            for idxSecond in idxFirst+1..<cards.count {
                XCTAssertNotEqual(cards[idxFirst].hashValue, cards[idxSecond].hashValue,
                                  "Card \(idxFirst) and \(idxSecond) had the same hash value")
            }
        }
            
    }
    
    func testEquality() {
        
        let card1 = Card(withNumber: Card.Feature.OptA,
                         andSymbol: Card.Feature.OptA,
                         andShading: Card.Feature.OptA,
                         andColor: Card.Feature.OptA)
        
        let card2 = Card(withNumber: Card.Feature.OptA,
                         andSymbol: Card.Feature.OptA,
                         andShading: Card.Feature.OptA,
                         andColor: Card.Feature.OptA)
    
        let card3 = Card(withNumber: Card.Feature.OptA,
                         andSymbol: Card.Feature.OptA,
                         andShading: Card.Feature.OptA,
                         andColor: Card.Feature.OptC)
        
        XCTAssertEqual(card1, card2)
        XCTAssertNotEqual(card2, card3)
        XCTAssertNotEqual(card1, card3)
    }

}
