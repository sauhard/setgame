//
//  CardUtilsTest.swift
//  SetGameTests
//
//  Created by Sauhard Sahi on 11/4/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import XCTest
@testable import SetGame

class CardUtilsTest: XCTestCase {

    func testMakeFreshDeck() {
        
        let deck = CardUtils.makeFreshDeck()
        
        // Should be 81 cards.
        XCTAssertEqual(81, deck.count)
        
        // None of the cards should be equal to each other.
        for i in 0..<deck.count {
            for j in 0..<deck.count {
                if (i == j) {
                    XCTAssertEqual(deck[i], deck[j])
                } else {
                    XCTAssertNotEqual(deck[i], deck[j])
                }
            }
        }
    }
    
    func testSet_Yes() {
        
        let card1 = Card(withNumber: Card.Feature.OptA,
                         andSymbol: Card.Feature.OptB,
                         andShading: Card.Feature.OptC,
                         andColor: Card.Feature.OptA)
        
        let card2 = Card(withNumber: Card.Feature.OptA,
                         andSymbol: Card.Feature.OptC,
                         andShading: Card.Feature.OptA,
                         andColor: Card.Feature.OptB)
        
        let card3 = Card(withNumber: Card.Feature.OptA,
                         andSymbol: Card.Feature.OptA,
                         andShading: Card.Feature.OptB,
                         andColor: Card.Feature.OptC)

        XCTAssertTrue(CardUtils.formsSet(card1: card1, card2: card2, card3: card3))
        XCTAssertTrue(CardUtils.formsSet(card1: card1, card2: card3, card3: card2))
        XCTAssertTrue(CardUtils.formsSet(card1: card2, card2: card1, card3: card3))
        XCTAssertTrue(CardUtils.formsSet(card1: card3, card2: card1, card3: card2))
        XCTAssertTrue(CardUtils.formsSet(card1: card3, card2: card2, card3: card1))
        XCTAssertTrue(CardUtils.formsSet(card1: card3, card2: card1, card3: card2))
    }
    
    func testSet_No() {
        
        let card1 = Card(withNumber: Card.Feature.OptA,
                         andSymbol: Card.Feature.OptB,
                         andShading: Card.Feature.OptC,
                         andColor: Card.Feature.OptA)
        
        let card2 = Card(withNumber: Card.Feature.OptA,
                         andSymbol: Card.Feature.OptC,
                         andShading: Card.Feature.OptA,
                         andColor: Card.Feature.OptB)
        
        let card3 = Card(withNumber: Card.Feature.OptB,
                         andSymbol: Card.Feature.OptA,
                         andShading: Card.Feature.OptB,
                         andColor: Card.Feature.OptC)
        
        XCTAssertFalse(CardUtils.formsSet(card1: card1, card2: card2, card3: card3))
        XCTAssertFalse(CardUtils.formsSet(card1: card1, card2: card3, card3: card2))
        XCTAssertFalse(CardUtils.formsSet(card1: card2, card2: card1, card3: card3))
        XCTAssertFalse(CardUtils.formsSet(card1: card3, card2: card1, card3: card2))
        XCTAssertFalse(CardUtils.formsSet(card1: card3, card2: card2, card3: card1))
        XCTAssertFalse(CardUtils.formsSet(card1: card3, card2: card1, card3: card2))
    }

}
