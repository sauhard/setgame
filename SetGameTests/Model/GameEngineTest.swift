//
//  GameEngineTest.swift
//  SetGameTests
//
//  Created by Sauhard Sahi on 11/12/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import XCTest

@testable import SetGame

class GameEngineTest: XCTestCase {

    // Delegate for testing callback execution.
    class TestDelegate : GameEventDelegate
    {
        public var firstDealtCardIndex: Int?
        public var lastDealtCardIndex: Int?
        public var selectedCardIndices: [Int] = []
        public var unselectedCardIndices: [Int] = []
        public var matchSucceededCardIndices: [Int] = []
        public var matchFailedCardIndices: [Int] = []
        public var shuffleOld2NewIndices: [Int] = []
        
        func didDeal(firstIndex: Int, lastIndex: Int) {
            self.firstDealtCardIndex = firstIndex
            self.lastDealtCardIndex = lastIndex
        }
        
        func didSelect(index: Int) {
            self.selectedCardIndices.append(index)
        }
        
        func didUnselect(index: Int) {
            self.unselectedCardIndices.append(index)
        }
        
        func didMatchAndRemove(indices: [Int]) {
            self.matchSucceededCardIndices.append(contentsOf: indices)
        }
        
        func didNotMatch(indices: [Int]) {
            self.matchFailedCardIndices.append(contentsOf: indices)
        }
        
        func didShuffle(old2NewIndices: [Int]) {
            self.shuffleOld2NewIndices = old2NewIndices
        }
    }
    
    func pickMatchingCards(withNumIndices numIndices: Int, andGame game: GameEngine)
        ->  (firstIndex: Int, secondIndex: Int, thirdIndex: Int)? {
        
        // For testing, pick three cards that form a set out of the ones provided.
        // Must be at least three cards in the set.
        if (numIndices < 3)
        {
            return nil
        }
        
        for i in 0..<numIndices
        {
            for j in i+1..<numIndices
            {
                for k in j+1..<numIndices
                {
                    if CardUtils.formsSet(card1: game.getDealtCard(index: i),
                                          card2: game.getDealtCard(index: j),
                                          card3: game.getDealtCard(index: k))
                    {
                        return (i, j, k)
                    }
                }
            }
        }
        
        return nil
    }
    
    func pickUnmatchingCards(withNumIndices numIndices: Int, andGame game: GameEngine)
        ->  (firstIndex: Int, secondIndex: Int, thirdIndex: Int)? {
        
        // For testing, pick three cards that don't form a set out of the ones provided.
        // Must be at least three cards in the set.
        if (numIndices < 3)
        {
            return nil
        }
        
        for i in 0..<numIndices
        {
            for j in i+1..<numIndices
            {
                for k in j+1..<numIndices
                {
                    if !CardUtils.formsSet(card1: game.getDealtCard(index: i),
                                           card2: game.getDealtCard(index: j),
                                           card3: game.getDealtCard(index: k))
                    {
                        return (i, j, k)
                    }
                }
            }
        }
        
        return nil
    }
    
    func testDeal() {
        
        // Instantiate test delegate and game engine.
        let testDelegate = TestDelegate()
        let gameEngine = GameEngine(delegate: testDelegate)
        
        // After dealing 1 card, there should be 1 card.
        gameEngine.deal(numCards: 1);
        XCTAssertNotNil(testDelegate.lastDealtCardIndex)
        XCTAssertEqual(0, testDelegate.firstDealtCardIndex!)
        XCTAssertEqual(0, testDelegate.lastDealtCardIndex!)
        
        // Dealing 0 cards shouldn't cause anything to happen.
        gameEngine.deal(numCards: 0)
        XCTAssertEqual(0, testDelegate.firstDealtCardIndex!)
        XCTAssertEqual(0, testDelegate.lastDealtCardIndex!)
        
        // Dealing a negative number of cards should also not cause anything to happen.
        gameEngine.deal(numCards: -5)
        XCTAssertEqual(0, testDelegate.firstDealtCardIndex!)
        XCTAssertEqual(0, testDelegate.lastDealtCardIndex!)
        
        // Dealing 15 cards should cause total count to go up to 16.
        gameEngine.deal(numCards: 15)
        XCTAssertEqual(1, testDelegate.firstDealtCardIndex!)
        XCTAssertEqual(15, testDelegate.lastDealtCardIndex)
        
        // Dealing 100 cards now should cause the total to be 81 (max number of cards available.
        gameEngine.deal(numCards: 100)
        XCTAssertEqual(16, testDelegate.firstDealtCardIndex!)
        XCTAssertEqual(80, testDelegate.lastDealtCardIndex!)
        
        // Dealing another 10 cards - nothing should happen. Max of 81 cards already dealt.
        gameEngine.deal(numCards: 10)
        XCTAssertEqual(16, testDelegate.firstDealtCardIndex!)
        XCTAssertEqual(80, testDelegate.lastDealtCardIndex!)
    }
    
    func testTouch() {
        
        // Instantiate test delegate and game engine.
        let testDelegate = TestDelegate()
        let gameEngine = GameEngine(delegate: testDelegate)
        
        // Deal 12 cards to start.
        gameEngine.deal(numCards: 12)
        
        // At first, both selected/unselected sets should be empty, and score should be 0.
        // Also, check the number of fresh & matched cards.
        XCTAssertTrue(testDelegate.selectedCardIndices.isEmpty)
        XCTAssertTrue(testDelegate.unselectedCardIndices.isEmpty)
        XCTAssertEqual(0, gameEngine.getScore())
        XCTAssertEqual(69, gameEngine.freshCardCount)
        XCTAssertEqual(0, gameEngine.matchedCardCount)
        
        
        // Touch the first card that was dealt, thereby selecting it.
        // We should see that the delegate to mark the card selected was called.
        // No change to score.
        gameEngine.touchDealtCard(atIndex: 0)
        XCTAssertEqual(1, testDelegate.selectedCardIndices.count)
        XCTAssertEqual(testDelegate.selectedCardIndices[0], 0)
        XCTAssertTrue(testDelegate.unselectedCardIndices.isEmpty)
        XCTAssertEqual(0, gameEngine.getScore())
        XCTAssertEqual(69, gameEngine.freshCardCount)
        XCTAssertEqual(12, gameEngine.dealtCardCount)
        XCTAssertEqual(0, gameEngine.matchedCardCount)
        
        // Touch the same card again, thereby unselecting it.
        // We should see that the delegate to mark the card unselected was also called.
        // No change to score.
        gameEngine.touchDealtCard(atIndex: 0)
        XCTAssertEqual(1, testDelegate.selectedCardIndices.count)
        XCTAssertEqual(testDelegate.selectedCardIndices[0], 0)
        XCTAssertEqual(1, testDelegate.unselectedCardIndices.count)
        XCTAssertEqual(testDelegate.unselectedCardIndices[0], 0)
        XCTAssertEqual(0, gameEngine.getScore())
        XCTAssertEqual(69, gameEngine.freshCardCount)
        XCTAssertEqual(12, gameEngine.dealtCardCount)
        XCTAssertEqual(0, gameEngine.matchedCardCount)
    }
    
    func testFailedMatch() {
        
        // Instantiate test delegate and game engine.
        let testDelegate = TestDelegate()
        let gameEngine = GameEngine(delegate: testDelegate)
        
        // Deal 12 cards to start.
        gameEngine.deal(numCards: 12)
        var numDealtCards = 12;
        
        // Get indexes of 3 unmatching cards. Keep dealing 3 more cards until
        // we can find 3 unmatching cards.
        var losingCombo = pickUnmatchingCards(withNumIndices: testDelegate.lastDealtCardIndex! + 1, andGame: gameEngine)
        while (losingCombo == nil) {
            gameEngine.deal(numCards: 3)
            numDealtCards += 3
            losingCombo = pickUnmatchingCards(withNumIndices: testDelegate.lastDealtCardIndex! + 1, andGame: gameEngine)
        }
        
        // Touch the three cards that we know to be no good.
        gameEngine.touchDealtCard(atIndex: losingCombo!.firstIndex)
        gameEngine.touchDealtCard(atIndex: losingCombo!.secondIndex)
        gameEngine.touchDealtCard(atIndex: losingCombo!.thirdIndex)
    
        // Match should have failed. Delegate should show the failed match, the score should be -1,
        // and there should still be the same number of cards in the game engine's dealt set.
        XCTAssertEqual(3, testDelegate.matchFailedCardIndices.count)
        XCTAssertTrue(testDelegate.matchSucceededCardIndices.isEmpty)
        XCTAssertEqual(-1, gameEngine.getScore())
        XCTAssertEqual(69, gameEngine.freshCardCount)
        XCTAssertEqual(12, gameEngine.dealtCardCount)
        XCTAssertEqual(0, gameEngine.matchedCardCount)
        XCTAssertEqual(numDealtCards, testDelegate.lastDealtCardIndex! + 1)
    }
    
    func testSuccessfulMatch() {
        
        // Instantiate test delegate and game engine.
        let testDelegate = TestDelegate()
        let gameEngine = GameEngine(delegate: testDelegate)
        
        // Deal 12 cards to start.
        gameEngine.deal(numCards: 12)
        var numDealtCards = 12
        
        // Get indexes of 3 matching cards. Keep dealing 3 more cards until
        // we can find 3 matching cards.
        var winningCombo = pickMatchingCards(withNumIndices: testDelegate.lastDealtCardIndex! + 1, andGame: gameEngine)
        while (winningCombo == nil) {
            gameEngine.deal(numCards: 3)
            numDealtCards += 3
            winningCombo = pickMatchingCards(withNumIndices: testDelegate.lastDealtCardIndex! + 1, andGame: gameEngine)
        }
        
        // Touch the three cards that we know to be a real match.
        gameEngine.touchDealtCard(atIndex: winningCombo!.firstIndex)
        gameEngine.touchDealtCard(atIndex: winningCombo!.secondIndex)
        gameEngine.touchDealtCard(atIndex: winningCombo!.thirdIndex)
        
        // Match should have succeeded. Delegate should show the successful match, the score should be 1,
        // and there should be 3 fewer cards in the game engine's dealt set, than before the successful match.
        XCTAssertEqual(3, testDelegate.matchSucceededCardIndices.count)
        XCTAssertTrue(testDelegate.matchFailedCardIndices.isEmpty)
        XCTAssertEqual(1, gameEngine.getScore())
        XCTAssertEqual(numDealtCards - 3, testDelegate.lastDealtCardIndex! + 1 - testDelegate.matchSucceededCardIndices.count)
        XCTAssertEqual(69, gameEngine.freshCardCount)
        XCTAssertEqual(9, gameEngine.dealtCardCount)
        XCTAssertEqual(3, gameEngine.matchedCardCount)
    }
    
    func testSelectionPreservedOnShuffle() {
        
        // Instantiate test delegate and game engine.
        let testDelegate = TestDelegate()
        let gameEngine = GameEngine(delegate: testDelegate)
        
        // Deal 12 cards to start.
        gameEngine.deal(numCards: 12)
        
        // Select a couple of cards (#5 and #7).
        gameEngine.touchDealtCard(atIndex: 5)
        gameEngine.touchDealtCard(atIndex: 7)
        
        // Shuffle!
        gameEngine.shuffleDealtCards()
        
        testDelegate.selectedCardIndices.removeAll()
        
        // Touch wherever card #5 and #7 moved to. This should cause them to become unselected,
        // and no cards should currently be considered selected.
        gameEngine.touchDealtCard(atIndex: testDelegate.shuffleOld2NewIndices[5])
        gameEngine.touchDealtCard(atIndex: testDelegate.shuffleOld2NewIndices[7])
        
        XCTAssertTrue(testDelegate.selectedCardIndices.isEmpty)
        XCTAssertEqual(2, testDelegate.unselectedCardIndices.count)
        XCTAssertEqual(testDelegate.shuffleOld2NewIndices[5], testDelegate.unselectedCardIndices[0])
        XCTAssertEqual(testDelegate.shuffleOld2NewIndices[7], testDelegate.unselectedCardIndices[1])
        XCTAssertEqual(69, gameEngine.freshCardCount)
        XCTAssertEqual(12, gameEngine.dealtCardCount)
        XCTAssertEqual(0, gameEngine.matchedCardCount)
    }
}
