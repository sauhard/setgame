//
//  ControllerConstants.swift
//  SetGame
//
//  Created by Sauhard Sahi on 11/20/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import Foundation

class ControllerConstants {
    
    // Initial number of cards to deal
    public static let INITIAL_CARD_COUNT: Int = 12
    
    // How many cards to deal when more are requested
    public static let DEAL_MORE_CARD_COUNT: Int = 3
    
}
