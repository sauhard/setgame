//
//  ViewController.swift
//  SetGame
//
//  Created by Sauhard Sahi on 8/18/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, GameEventDelegate {
    
    // Connection for the fresh cards deck view
    @IBOutlet var freshCardsDeckView: DeckView!
    
    // Connection for the matched cards deck view
    @IBOutlet var matchedCardsDeckView: DeckView!
    
    // Connection for the card holder view
    @IBOutlet var cardHolderView: CardHolderView!
    
    // Connection for the 'New Game' button
    @IBOutlet weak var newGameButton: UIButton!
    
    // Connection for the score label
    @IBOutlet weak var scoreLabel: UILabel!
    
    // Game engine
    lazy var gameEngine = GameEngine(delegate: self)
    
    // Translate a card to a card view.
    func translateToView(card: Card) -> CardView {
    
        let cv = CardView()
        
        // Set color
        switch card.color {
            case .OptA:
                cv.color = .Green
            case .OptB:
                cv.color = .Purple
            case .OptC:
                cv.color = .Red
        }
        
        // Set number
        switch card.number {
            case .OptA:
                cv.number = .One
            case .OptB:
                cv.number = .Two
            case .OptC:
                cv.number = .Three
        }
        
        // Set shading
        switch card.shading {
            case .OptA:
                cv.shading = .Open
            case .OptB:
                cv.shading = .Solid
            case .OptC:
                cv.shading = .Striped
        }
        
        // Set symbol
        switch card.shape {
            case .OptA:
                cv.shape = .Diamond
            case .OptB:
                cv.shape = .Oval
            case .OptC:
                cv.shape = .Squiggle
        }
        
        // Add gesture recognizer for taps.
        cv.addGestureRecognizer(UITapGestureRecognizer(
            target: self, action: #selector(ViewController.didTapCardView(_:))
        ))
        
        return cv
    }
    
    // Called by the game engine upon dealing of cards.
    func didDeal(firstIndex: Int, lastIndex: Int) {
        
        struct Constants {
            
            // Time between cards
            static let TimeBetweenCards: TimeInterval = 0.05
        }
        
        // Add newly dealt cards to the holder. Do each of these on a timer.
        var loopCycle = 0
        
        for i in firstIndex...lastIndex {
            
            loopCycle += 1
            
            Timer.scheduledTimer(
                withTimeInterval: Constants.TimeBetweenCards * TimeInterval(loopCycle),
                repeats: false,
                block: { [weak gameEngine] _ in
                    
                    // If the game engine reference has disappeared (due to a reset),
                    // this timer should not execute its logic as it was for the previous game.
                    if (gameEngine == nil) {
                        return
                    }
                    
                    // Create a card view.
                    let cardView = self.translateToView(card: self.gameEngine.getDealtCard(index: i))
                    
                    // Stick it into the card holder.
                    self.cardHolderView.add(cardView: cardView)
                    
                    // Subtract from fresh cards.
                    self.freshCardsDeckView.numCards -= 1
                    
                }
            )
        }
    }
    
    // Called by the game engine to confirm when the user has selected a card.
    func didSelect(index: Int) {
        
        // Lookup the card button, and mark it selected.
        self.cardHolderView.at(index: index)!.selectionState = .Selected
    }
    
    // Called by the game engine to confirm when the user has deselected a card.
    func didUnselect(index: Int) {
        
        // Lookup the card button, and mark it as unselected.
        self.cardHolderView.at(index: index)!.selectionState = .Unselected
    }
    
    // Called by the game engine to confirm when the user has successfully matched cards.
    func didMatchAndRemove(indices: [Int]) {
        
        // Show success and remove the cards at the given indices.
        self.cardHolderView.showSuccessAndRemove(indices: indices)
        
        // Update visible progress.
        self.updateProgress()
    }
    
    // Called by the game engine to confirm when the user has failed to match cards.
    func didNotMatch(indices: [Int]) {
        
        // Show failure on the cards at the given indices.
        self.cardHolderView.showFailure(atIndices: indices)
        
        // Update visible progress.
        self.updateProgress()
    }
    
    // Called by the game engine upon a successful shuffle.
    func didShuffle(old2NewIndices: [Int]) {
        
        // Reflect the shuffling in the card holder.
        self.cardHolderView.reflectShuffle(old2NewIndices: old2NewIndices)
    }
    
    // Called when the user taps a card view.
    @objc func didTapCardView(_ sender: UITapGestureRecognizer) {
        
        switch (sender.state) {
            
            // If the tap has ended, we'll continue down to the code below.
            case .ended:
                break
            
            // Filter out events that are not "ended".
            default:
                return
        }
        
        // Unwrap sender's view and make sure it's a real CardView, otherwise this is a phanotm event.
        if let cardView = sender.view as? CardView
        {
            // Get index of the button that was touched, and tell the game engine.
            if let index = self.cardHolderView.index(of: cardView)
            {
                self.gameEngine.touchDealtCard(atIndex: index)
            }
        }
    }
    
    // Called when the user swipes down.
    @objc func didSwipeDownGesture(_ sender: UISwipeGestureRecognizer) {
        
        struct Constants {
            // What to flash with to let the user know they are doing the gesture
            static let FlashColor: UIColor = UIColor.gray.withAlphaComponent(0.25)
            
            // How long should the animation last
            static let AnimateTime: TimeInterval = 0.25
        }
        
        switch (sender.state) {
            
            case .ended:
            
                // Flash the background...
                UIView.animate(
                  withDuration: Constants.AnimateTime,
                  animations: {
                    self.view.backgroundColor = Constants.FlashColor
                  },
                  completion: { _ in
                    
                    // Then back to clear.
                    UIView.animate(
                      withDuration: Constants.AnimateTime,
                      animations: {
                        self.view.backgroundColor = UIColor.clear
                      }
                    )
                  }
                )
            
                // While the animation is happening, deal more cards from the game engine!
                self.gameEngine.deal(numCards: ControllerConstants.DEAL_MORE_CARD_COUNT)
            
            default:
                break
        }
    }
    
    // Called when the user does a shuffle gesture.
    @objc func didRotationGesture(_ sender: UIRotationGestureRecognizer) {
        
        struct Constants {
            // What to flash with to let the user know they are doing the gesture
            static let FlashColor: UIColor = UIColor.purple.withAlphaComponent(0.25)
            
            // How long should the animation last
            static let AnimateTime: TimeInterval = 0.25
        }
        
        switch (sender.state) {
        
            case .ended:
                
                // Flash the background...
                UIView.animate(
                  withDuration: Constants.AnimateTime,
                  animations: {
                    self.view.backgroundColor = Constants.FlashColor
                  },
                  completion: { _ in
                    
                    // Then back to clear.
                    UIView.animate(
                      withDuration: Constants.AnimateTime,
                      animations: {
                        self.view.backgroundColor = UIColor.clear
                     }
                    )
                  }
                )
            
                // Shuffle dealt cards.
                self.gameEngine.shuffleDealtCards()
            
            default:
                break
            
        }
    }
    
    // Called when the user does a tap
    @objc func didTapOnFreshCardsGesture(_ sender: UITapGestureRecognizer) {
        
        switch (sender.state) {
            
            case .ended:
                // Ask for 3 more cards.
                self.gameEngine.deal(numCards: ControllerConstants.DEAL_MORE_CARD_COUNT)
            
            default:
                break
            
        }
        
    }
    
    // Called when the start new game button is touched.
    @IBAction func touchNewGameButton() {
        
        // Start over.
        self.startFreshGame()
    }
    
    // Present a new game to the user.
    func startFreshGame() {
        
        // Clear card holder and make a new game engine.
        self.cardHolderView.clear()
        self.gameEngine = GameEngine(delegate: self)
        
        // Update visible information.
        self.updateProgress()
        
        // Start by dealing 12 cards.
        self.gameEngine.deal(numCards: ControllerConstants.INITIAL_CARD_COUNT)
    }
    
    // Update info area at the bottom of the app.
    func updateProgress() {
        
        // Update fresh card deck view.
        self.freshCardsDeckView.numCards = self.gameEngine.freshCardCount
        
        // Update matched card deck view.
        self.matchedCardsDeckView.numCards = self.gameEngine.matchedCardCount
        
        // Show current score.
        self.scoreLabel.text = "Score: \(self.gameEngine.getScore())"
    }
    
    override func viewDidLoad() {
        
        // Superclass handler.
        super.viewDidLoad()
        
        // Handle swipe down on the card holder view.
        let swipeDown = UISwipeGestureRecognizer(
            target: self, action: #selector(self.didSwipeDownGesture(_:))
        )
        swipeDown.direction = .down
        self.cardHolderView.addGestureRecognizer(swipeDown)
        
        // Handle rotation on the card holder view.
        let rotate = UIRotationGestureRecognizer(
            target: self, action: #selector(self.didRotationGesture(_:))
        )
        self.cardHolderView.addGestureRecognizer(rotate)
        
        // Handle tap on the fresh cards deck.
        let tap = UITapGestureRecognizer(
            target: self, action: #selector(self.didTapOnFreshCardsGesture(_:))
        )
        self.freshCardsDeckView.addGestureRecognizer(tap)
        
        // Show current state.
        self.updateProgress()
        
        // Start a new game.
        self.startFreshGame()
    }

}

