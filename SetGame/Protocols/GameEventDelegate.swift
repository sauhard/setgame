//
//  GameEventDelegate.swift
//  SetGame
//
//  Created by Sauhard Sahi on 11/4/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import Foundation

protocol GameEventDelegate: class {
    
    // Called when new cards have been dealt.
    // The first index corresponds to the first new card dealt,
    // and the last index corresponds to the last new card dealt.
    func didDeal(firstIndex: Int, lastIndex: Int)
    
    // Called when a card has been selected.
    func didSelect(index: Int)
    
    // Called when a card has been unselectd.
    func didUnselect(index: Int)
    
    // Called when the cards at the provided indices have matched
    // and been removed from the game.
    func didMatchAndRemove(indices: [Int])
    
    // Called when the cards at the provided indices have not matched
    // and are still in the game.
    func didNotMatch(indices: [Int])
    
    // Called when all of the currently dealt cards were shuffled,
    // along with a mapping of old indices to new indices.
    func didShuffle(old2NewIndices: [Int])
}
