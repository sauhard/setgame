//
//  Card.swift
//  SetGame
//
//  Created by Sauhard Sahi on 8/18/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import Foundation

class Card: Hashable, CustomStringConvertible {
    
    // Three choices for each feature of a card - call them Options A through C
    enum Feature: Int {
        case OptA = 0
        case OptB = 1
        case OptC = 2
        
        // Return the three options above.
        static func choices() -> [Feature] {
            return [OptA, OptB, OptC]
        }
        
        // Return the number of options available.
        static func numChoices() -> Int {
            return choices().count
        }
    }
    
    // Card's cardinality setting
    let number: Feature
    
    // Card's shape setting
    let shape: Feature
    
    // Card's shading setting
    let shading: Feature
    
    // Card's color setting
    let color: Feature
    
    // Return how many features there are on a card.
    static func numFeatures() -> Int {
        return 4
    }
    
    // Initialize a card with the given number, symbol, shading, and color.
    init(withNumber number: Feature, andSymbol symbol: Feature, andShading shading: Feature, andColor color: Feature) {
        self.number = number
        self.shape = symbol
        self.shading = shading
        self.color = color
    }
    
    // Card's hash - based on the four immutable properties above
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.number)
        hasher.combine(self.shape)
        hasher.combine(self.shading)
        hasher.combine(self.color)
    }
    
    
    // Two cards are considered equal on the basis of their hashes tying out.
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    // Description
    public var description: String {
        return "number: \(self.number), symbol: \(self.shape), shading: \(self.shading), color: \(self.color)"
    }
}
