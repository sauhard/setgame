//
//  GameEngine.swift
//  SetGame
//
//  Created by Sauhard Sahi on 8/18/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import Foundation

class GameEngine {
    
    // Fresh cards that have not been dealt, and thus should not be visible yet
    private var freshCards: [Card]
    
    // Cards that have been dealt, and thus should be visible
    private var dealtCards: [Card]
    
    // Indices of the cards that have been chosen by the user, out of the currently dealt cards
    private var selectedDealtCardIndices: Set<Int>
    
    // The user's score so far
    private var score: Int
    
    // Fresh card count.
    public var freshCardCount: Int {
        return freshCards.count
    }
    
    // Dealt card count.
    public var dealtCardCount: Int {
        return dealtCards.count
    }
    
    // Matched card count. This can be inferred from how many cards there are in a game,
    // minus the amount appearing in the fresh & dealt sets.
    public var matchedCardCount: Int {
        return CardUtils.NUM_CARDS_IN_GAME - freshCardCount - dealtCardCount
    }

    // Delegate to handle events in the game
    private weak var delegate: GameEventDelegate?
    
    // Static variable to indicate how many cards a user must choose before
    // they have selected a set.
    private static var _CARD_SELECTION_THRESHOLD: Int = 3
    
    // Create a new game engine with a fresh deck of cards.
    init(delegate: GameEventDelegate) {
        
        // Deal fresh cards.
        self.freshCards = CardUtils.makeFreshDeck()
        self.freshCards.shuffle()
        
        // Make the other collections empty.
        self.dealtCards = []
        self.selectedDealtCardIndices = []
        
        // Score starts at 0.
        self.score = 0
        
        // Set delegate.
        self.delegate = delegate
    }
    
    // Determine whether there are cards remaining.
    func hasMoreCards() -> Bool {
        return !self.freshCards.isEmpty
    }
    
    // Get the current score.
    func getScore() -> Int {
        return self.score
    }
    
    // Deal the specified number of cards, up to cardCount.
    func deal(numCards: Int) {
        
        // If numCards is non-positive, can't do anything!
        if (numCards <= 0) {
            return
        }
        
        // Maintain cards to return for delegate.
        var freshCards: [Card] = [];
        
        // Get number of cards before dealing.
        let numAlreadyDealtCards = self.dealtCards.count
        
        // Transfer the specified number of cards from the fresh pile into the
        // dealt pile. Do this from the back of the fresh cards pile, to avoid
        // efficiency loss from having to bubble down the remaining cards.
        for _ in 0..<numCards {
            if let card = self.freshCards.popLast() {
                freshCards.append(card)
            }
        }
        
        // Add freshly dealt cards to the registry of dealt cards.
        self.dealtCards.append(contentsOf: freshCards)
        
        // Tell the delegate about the freshly dealt cards,
        // if there were any.
        if (self.dealtCards.count != numAlreadyDealtCards)
        {
            self.delegate?.didDeal(
                firstIndex: numAlreadyDealtCards,
                lastIndex: self.dealtCards.count - 1)
        }
    }
    
    // Get dealt card at the given index.
    func getDealtCard(index: Int) -> Card{
        return self.dealtCards[index]
    }
    
    // Touch a dealt card. This will toggle its selected/deselected state.
    func touchDealtCard(atIndex index: Int) {
        
        // Ensure card is dealt, otherwise this is not a valid action.
        if self.dealtCards.isInRange(index: index) {
            
            // Is the card already selected? Unselect it and inform delegate.
            if self.selectedDealtCardIndices.remove(index) != nil {
                delegate?.didUnselect(index: index)
            }
            
            // Otherwise, select it, inform delegate, and handle
            // selection event (which may opt to emit further events).
            else {
                self.selectedDealtCardIndices.insert(index)
                self.delegate?.didSelect(index: index)
                self._handleSelections()
            }
        }
    }
    
    // Shuffle the currently dealt cards and notify via delegate
    // the details about the shuffle.
    func shuffleDealtCards() {
        
        // Pick a new ordering of indices.
        // Subscript is the old index, value is the new index!
        let old2NewIndices = Array(0..<self.dealtCards.count).shuffled()
        
        // Replace selected card indices with their new indices.
        self.selectedDealtCardIndices = Set(self.selectedDealtCardIndices.map( { old2NewIndices[$0] }))
        
        // Update dealt cards and notify delegate.
        self.dealtCards.rearrange(old2NewIndices: old2NewIndices)
        self.delegate?.didShuffle(old2NewIndices: old2NewIndices)
    }
    
    // Internal function to determine whether all of the cards selected so far
    // form a set. There are three possible outcomes:
    //
    // (1) not enough cards selected yet to determine whether a set has been formed
    // (2) enough cards have been selected, but it is not a set (penalize user)
    // (3) enough cards have been selected, and it is a set (reward user)
    //
    // In the first case, nothing will happen. In the latter two cases, the user's
    // score will be updated, and an appropriate event will be emitted back to the controller.
    private func _handleSelections() {
        
        // Don't do anything if we're not currently at the magic number!
        if (self.selectedDealtCardIndices.count != GameEngine._CARD_SELECTION_THRESHOLD) {
            return
        }
        
        // Get the three cards (indices and actual cards themselves).
        let cardIndices = self.selectedDealtCardIndices.map{$0}
        let cards = cardIndices.map{self.dealtCards[$0]}
        
        // If we formed a set, yay! Increment the score and tell the delegate about the success.
        if (CardUtils.formsSet(card1: cards[0], card2: cards[1], card3: cards[2])) {
            self.score += 1
            
            // Remove from dealt cards.
            self.dealtCards.removeAll(where: { cards.contains($0) })
            
            // Now tell the delegate (if it iterates, it won't see the matched cards anymore).
            self.delegate?.didMatchAndRemove(indices: cardIndices)
        }
        
        // Otherwise, oops. Decrement the score, tell the delegate about the failure,
        // and unselect all cards.
        else {
            self.score -= 1
            delegate?.didNotMatch(indices: cardIndices)
        }
        
        // In either success or failure scenario, clear the selectedCards,
        // because they're no longer selected.
        self.selectedDealtCardIndices.removeAll()
    }
}
