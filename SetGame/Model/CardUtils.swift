//
//  CardUtils.swift
//  SetGame
//
//  Created by Sauhard Sahi on 8/26/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import Foundation

class CardUtils {
 
    // Return how many cards there are in a game.
    static let NUM_CARDS_IN_GAME: Int = 81
    
    // Returns a fresh deck of cards.
    static func makeFreshDeck() -> [Card] {
        
        var cards = [Card]();
        cards.reserveCapacity(Card.numFeatures() * Card.Feature.numChoices());
        
        // Iterate over all possible permutations of features and their possibilities,
        // add to the cards array.
        for number in Card.Feature.choices() {
            for symbol in Card.Feature.choices() {
                for shading in Card.Feature.choices() {
                    for color in Card.Feature.choices() {
                        
                        cards.append(Card(withNumber: number,
                                          andSymbol: symbol,
                                          andShading: shading,
                                          andColor: color))
                    }
                }
            }
        }
        
        return cards
    }
    
    // Check whether the three cards specified form a set.
    static func formsSet(card1: Card, card2: Card, card3: Card) -> Bool {
        
        return CardUtils._areAllSameOrDifferent(f1: card1.color,
                                                f2: card2.color,
                                                f3: card3.color)
            
            && CardUtils._areAllSameOrDifferent(f1: card1.number,
                                                f2: card2.number,
                                                f3: card3.number)
            
            && CardUtils._areAllSameOrDifferent(f1: card1.shading,
                                                f2: card2.shading,
                                                f3: card3.shading)
            
            && CardUtils._areAllSameOrDifferent(f1: card1.shape,
                                                f2: card2.shape,
                                                f3: card3.shape)
    }
    
    // Check whether three features are all the same or are all different.
    private static func _areAllSameOrDifferent(f1: Card.Feature, f2: Card.Feature, f3: Card.Feature) -> Bool {
        return ((f1 == f2) && (f2 == f3)) || ((f1 != f2) && (f2 != f3) && (f1 != f3))
    }
    
}
