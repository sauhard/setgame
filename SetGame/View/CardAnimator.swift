//
//  CardAnimator.swift
//  SetGame
//
//  Created by Sauhard Sahi on 12/26/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import Foundation
import UIKit

class CardAnimator
{
    struct TimeConstants {
        static let SlideIn: TimeInterval = 0.5
        static let Flip: TimeInterval = 0.2
        static let Move: TimeInterval = 0.3
        static let Select: TimeInterval = 0.25
    }
    
    // Slides in a cardView, from its current frame to another one.
    // The card goes into the flipped-down state and becomes flipped-up during the animation.
    static func slideIn(cardView: CardView, to toFrame: CGRect) {
        
        // Start with the face state down before animation begins.
        cardView.faceState = .Down
        
        // Move into position
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: TimeConstants.SlideIn,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                cardView.frame = toFrame
            },
            completion: { _ in
                
                // Flip over
                UIView.transition(with: cardView,
                    duration: TimeConstants.Flip,
                    options: .transitionFlipFromLeft,
                    animations: {
                        cardView.faceState = .Up
                    }
                )
            }
        )
    }
    
    // Moves an existing card from its current position to another point.
    // No change is made to the card's up/down state while doing so.
    static func move(cardView: CardView, to toFrame: CGRect) {
        
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: TimeConstants.Move,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                cardView.frame = toFrame
            }
        )
    }
    
    // Change selection.
    static func changeSelection(cardView: CardView, to toSelection: CardView.SelectionState,
                                andThen thenFunc: (() -> Void)? = nil) {
        
        UIView.transition(with: cardView,
            duration: TimeConstants.Select,
            options: .transitionCrossDissolve,
            animations: {
                cardView.selectionState = toSelection
            },
            completion: { _ in
                thenFunc?()
            }
        )
    }
    
    // Slides out the cards, from their current frame to the new one.
    // The following events will happen:
    // (1) All cards move in a random direction.
    // (2) As they collide into each other, they will bounce away.
    // (3) Upon hitting a wall, they will fall into the bottom and fade out.
    // After fading out, an optional "andThen" can be called to do any final processing needed.
    static func slideOut(cardViews: [CardView], from cardHolder: CardHolderView,
         andThen thenFunc: (() -> Void)? = nil) {

        
        // Bring each card to the front, then flip it down.
        cardViews.forEach({cardView in
            
            cardView.bringSubviewToFront(cardHolder)
            
            UIView.transition(with: cardView,
              duration: TimeConstants.Flip,
              options: .transitionFlipFromLeft,
              animations: {
                cardView.faceState = .Down
              }
            )
        })
        
        // Create an animator, it will manage its own lifecycle.
        let _ = SlideOutDynamicAnimator(cardViews: cardViews, cardHolder: cardHolder, andThen: thenFunc)
    }
}
