//
//  DeckView.swift
//  SetGame
//
//  Created by Sauhard Sahi on 12/13/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import UIKit

@IBDesignable
class DeckView: UIView {

    // How many cards to draw in the deck
    @IBInspectable
    var numCards: Int = 0 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    // What color should the cards be filled with
    var cardFillColor: UIColor = UIColor.white {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    // What color the border should be
    var borderColor: UIColor = UIColor.white {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    // UIViews for the individual cards
    private var deckCards: [UIView] = []
    
    // Layout the cards! Only draw new ones that haven't been shown yet, and only remove ones that were needed.
    override func layoutSubviews() {
        
        // Settings
        struct Constants {
            static let BorderWidth: CGFloat = 1.0
        }
        
        // More cards in the deck than desired - remove the extra
        if (self.deckCards.count > self.numCards) {
            for _ in 0..<(self.deckCards.count - self.numCards) {
                self.deckCards.popLast()?.removeFromSuperview()
            }
        }
        
        // Fewer cards in the deck than desired - add more to cover the amount of the shortage
        else if (self.deckCards.count < self.numCards) {
            for _ in (0..<(self.numCards - self.deckCards.count)) {
                let cardView = UIView()
                cardView.frame = self.nextCardFrame
                cardView.backgroundColor = self.cardFillColor
                self.addSubview(cardView)
                self.deckCards.append(cardView)
            }
        }
        
        // Ensure correct color and frame.
        for i in 0..<self.deckCards.count {
            self.deckCards[i].backgroundColor = self.cardFillColor
            self.deckCards[i].frame = self.getCardFrame(atIndex: i)
        }
        
        // Set border color.
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = Constants.BorderWidth
        
        super.layoutSubviews()
    }
}

extension DeckView {
    
    // Parameters to control drawing of the deck, relative to width of the deck
    // Note that the following should hold true:
    // (CARD_WIDTH * CardUtils.NUM_CARDS_IN_GAME) + (PADDING_WIDTH * (CardUtils.NUM_CARDS_IN_GAME - 1)) <= 1
    // Otherwise, some of the cards will overflow past the drawable area on the right side of the view.
    struct RelativeToDeckWidth {
        
        // How wide a card should be, relative to the deck width
        static let CARD_WIDTH: CGFloat = 0.008
        
        // How much padding there should be, relative to the deck width
        static let PADDING_WIDTH: CGFloat = 0.0044
    }
    
    // Card height
    private var cardHeight: CGFloat {
        return self.bounds.height
    }
    
    // Card width
    private var cardWidth: CGFloat {
        return RelativeToDeckWidth.CARD_WIDTH * self.bounds.width
    }
    
    // Padding between cards
    private var paddingBetweenCards: CGFloat {
        return RelativeToDeckWidth.PADDING_WIDTH * self.bounds.width
    }
    
    // Get the frame that should be used for a card at the given index.
    func getCardFrame(atIndex index: Int) -> CGRect {
        return CGRect(
            x: (self.cardWidth + self.paddingBetweenCards) * CGFloat(index),
            y: 0, width: self.cardWidth, height: self.cardHeight
        )
    }
    
    // Get the card frame that should be used for the next card.
    var nextCardFrame: CGRect {
        return self.getCardFrame(atIndex: self.deckCards.count)
    }
    
}
