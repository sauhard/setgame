//
//  CardHolderView.swift
//  SetGame
//
//  Created by Sauhard Sahi on 11/25/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import UIKit

class CardHolderView: UIView {
    
    // Constants
    struct Constants {
        static let CardAspectRatio: CGFloat = 5.0/8.0
        static let InsetPoints: CGFloat = 2.0
    }
    
    // CardViews that need to be shown
    private var cardViews: [CardView] = []
    
    // Grid to figure out how to arrange the cards
    private lazy var grid: Grid = Grid(layout: Grid.Layout.aspectRatio(Constants.CardAspectRatio), frame: self.bounds)
    
    // Add a CardView.
    func add(cardView: CardView) {
        
        self.cardViews.append(cardView)
        self.setNeedsLayout()
    }
    
    // Get number of cards.
    func count() -> Int {
        return self.cardViews.count
    }
    
    // Get a CardView at a given index.
    func at(index: Int) -> CardView? {
        return self.cardViews[index]
    }
    
    // Get index of a given CardView.
    func index(of cardView: CardView) -> Int? {
        return self.cardViews.firstIndex(of: cardView)
    }
    
    // Show success, and then run the desired lambda.
    func showSuccessAndRemove(indices: [Int]) {
        
        // First get the card views at the given indices.
        let successCardViews: [CardView] = indices.compactMap( { self.at(index: $0) })
        
        // Mark each card as being in a successful state.
        successCardViews.forEach({ cardView in
            
            CardAnimator.changeSelection(
                cardView: cardView, to: .MatchSucceeded)
            
        })
        
        // Slide them out.
        CardAnimator.slideOut(cardViews: successCardViews, from: self,
            andThen: {
                
                // Then remove each card from the superview.
                successCardViews.forEach({ cardView in
                    cardView.removeFromSuperview()
                    self.cardViews.removeAll(where: {cardView == $0})
                    self.setNeedsLayout()
                })
                
            }
        )
    }
    
    // Show failure at the indices provided.
    func showFailure(atIndices indices: [Int]) {
        
        // First get the card views at the given indices.
        let failureCardViews: [CardView] = indices.compactMap( { self.at(index: $0) })
        
        // Briefly show failure state, then revert to unselected state.
        failureCardViews.forEach({ cardView in
            
            CardAnimator.changeSelection(
                cardView: cardView, to: .MatchFailed,
                andThen: {
                    
                    CardAnimator.changeSelection(
                        cardView: cardView, to: .Unselected)
                    
                }
            )
        })
    }
    
    // Re-layout the cards based on the shuffle provided.
    func reflectShuffle(old2NewIndices: [Int]) {
        self.cardViews.rearrange(old2NewIndices: old2NewIndices)
        self.setNeedsLayout()
    }
    
    // Remove all CardViews.
    func clear() {
        
        // Important to take them all out of their superviews.
        self.cardViews.forEach( { $0.removeFromSuperview() })
        
        // Clear array.
        self.cardViews.removeAll()
        self.setNeedsLayout()
    }
    
    // Determine how many cells the grid should have.
    // Try to avoid dropping below a sensible minimum.
    func getCellCount() -> Int {
        return max(ControllerConstants.INITIAL_CARD_COUNT, cardViews.count)
    }
    
    // Make stuff look pretty
    override func layoutSubviews()
    {
        // Update card count and frame size if necessary. Check if they've changed before setting,
        // to avoid having the grid perform unnecessary recalculations under the hood.
        if (self.grid.cellCount != getCellCount()) {
            self.grid.cellCount = getCellCount()
        }
        if (self.grid.frame != self.bounds) {
            self.grid.frame = self.bounds
        }
        
        // Add new cards.
        for cardViewIndex in 0..<self.cardViews.count {
            
            let cardView = self.cardViews[cardViewIndex]
            
            // Pick a new frame based on what the grid says (remove a little space for padding).
            let calculatedFrame = self.grid[cardViewIndex]!
                .insetBy(dx: Constants.InsetPoints, dy: Constants.InsetPoints)
            
            // Add the card view if it's not already in the subview.
            if (cardView.superview != self) {
                cardView.removeFromSuperview()
            }
            if (cardView.superview == nil) {
                self.addSubview(cardView)
                
                // Start the card at the top.
                cardView.frame = CGRect(
                    x: bounds.midX - (calculatedFrame.width / 2),
                    y: 0,
                    width: calculatedFrame.width,
                    height: calculatedFrame.height)
                
                // Slide it into its final position.
                CardAnimator.slideIn(cardView: cardView, to: calculatedFrame)
                
            }
            
            // Move if needed
            if (cardView.frame != calculatedFrame) {
                CardAnimator.move(cardView: cardView, to: calculatedFrame)
            }
            
            // Ask the card to be drawn because it may be the first time we are showing it,
            // or its dimensions may have been recalculated since last time it was drawn.
            cardView.setNeedsDisplay()
        }
        
        super.layoutSubviews()
    }

}
