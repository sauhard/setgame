//
//  SlideOutDynamicAnimator.swift
//  SetGame
//
//  Created by Sauhard Sahi on 12/30/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import UIKit

class SlideOutDynamicAnimator : NSObject {

    // Each dynamic animator lives until it settles down, then it will remove itself from memory.
    private static var REGISTRY: Set<SlideOutDynamicAnimator> = []
    
    // The actual animator, encapsulated
    private let animator: UIDynamicAnimator
    
    // Function to run upon completion of animation
    private var andThenFunc: (() -> Void)?
    
    // Constants to help out with things
    struct Constants {
        
        // Valid range for angles of the initial push
        static let PushAngleRange: Range<CGFloat> = 0..<2*CGFloat.pi
        
        // Push magnitude
        static let PushMagnitude: CGFloat = 20.0
        
        // Elasticity for each item
        static let CardElasticity: CGFloat = 1.0
        
        // Friction for each card
        static let CardFriction: CGFloat = 0.0
        
        // Snap damping
        static let SnapDampingFactor: CGFloat = 0.5
        
        // Cycles before adding snap
        static let CyclesBeforeSnap: Int = 20
        
    }
    
    // Instantiate with cards and a card holder.
    init(cardViews: [CardView], cardHolder: CardHolderView, andThen: (() -> Void)? = nil)
    {
        // Create a dynamic animator using the card holder.
        self.animator = UIDynamicAnimator(referenceView: cardHolder)
        
        // Save function to run upon completion.
        self.andThenFunc = andThen
        
        // Call NSObject constructor
        super.init()
        
        // Set self as delegate.
        animator.delegate = self
        
        // Save ourselves in the registry so we don't go out of scope
        // before the animation reaches stasis.
        SlideOutDynamicAnimator.REGISTRY.insert(self)
        
        // Setup collision behavior.
        self.animator.addBehavior({
            let collision = UICollisionBehavior(items: cardViews)
            collision.setTranslatesReferenceBoundsIntoBoundary(with: .zero)
            collision.collisionMode = .boundaries
            return collision
        }())
        
        
        // Add push on each card.
        for cardView in cardViews {
            animator.addBehavior({
                let push = UIPushBehavior(items: [cardView], mode: .instantaneous)
                
                push.angle = CGFloat.random(in: Constants.PushAngleRange)
                push.magnitude = Constants.PushMagnitude
                push.action = { [unowned animator] in
                    animator.removeBehavior(push)
                }
                
                return push
            }())
        }
        
        // Disallow rotation on each card.
        self.animator.addBehavior({
            let noRotation = UIDynamicItemBehavior(items: cardViews)
            noRotation.allowsRotation = false
            return noRotation
        }())
        
        // Add bounce behavior on each card.
        self.animator.addBehavior({
            let metaBehavior = UIDynamicItemBehavior(items: cardViews)
            metaBehavior.elasticity = Constants.CardElasticity
            metaBehavior.friction = Constants.CardFriction
            
            var cycleCount = 0
            
            metaBehavior.action = { [unowned metaBehavior, unowned animator] in
                
                // Increment cycles.
                cycleCount += 1
                    
                // Once certain cycle count has been reached, add snap behavior.
                if (cycleCount >= Constants.CyclesBeforeSnap)
                {
                    // Snap behavior per card
                    cardViews.forEach({ cardView in
                        animator.addBehavior({
                            let snap = UISnapBehavior(item: cardView, snapTo: self.getSnapPoint(forCardView: cardView))
                            snap.damping = Constants.SnapDampingFactor
                            return snap
                        }())
                    })
                    // We are done!
                    animator.removeBehavior(metaBehavior)
                    
                }
            }
            return metaBehavior
        }())
    }
    
    // Get the point to which cards should snap.
    private func getSnapPoint(forCardView cardView: CardView) -> CGPoint {
        return CGPoint(
            x: self.animator.referenceView!.bounds.midX,
            y: animator.referenceView!.bounds.maxY
        )
    }
}

extension SlideOutDynamicAnimator : UIDynamicAnimatorDelegate {
    
    func dynamicAnimatorDidPause(_: UIDynamicAnimator) {
        
        // Remove any behaviors that are still left.
        self.animator.removeAllBehaviors()
        
        // Run desired logic, since we are now at stasis.
        self.andThenFunc?()
        
        // Remove animator from the registry.
        // This allows it to be freed from memory.
        SlideOutDynamicAnimator.REGISTRY.remove(self)
    }

}
