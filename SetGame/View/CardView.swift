//
//  CardView.swift
//  SetGame
//
//  Created by Sauhard Sahi on 11/23/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import UIKit

@IBDesignable
class CardView: UIView {

    // Number choices
    enum Number : Int {
        case One = 1
        case Two = 2
        case Three = 3
    }
    var number: Number = .One {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    // Shape choices
    enum Shape : String {
        case Diamond
        case Squiggle
        case Oval
    }
    var shape: Shape = .Diamond {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    // Shading choices
    enum Shading : String {
        case Solid
        case Striped
        case Open
    }
    var shading: Shading = .Solid {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    // Color
    enum Color : String {
        case Red
        case Green
        case Purple
    }
    var color: Color = .Red {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    // Current selection state of this card
    enum SelectionState : String {
        case Unselected
        case Selected
        case MatchFailed
        case MatchSucceeded
    }
    var selectionState: SelectionState = .Unselected {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    // Current face state of this card
    enum FaceState : String {
        case Down
        case Up
    }
    var faceState: FaceState = .Down {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    // Get the path for a diamond.
    private static func getDiamondShapePath(inRect rect: CGRect) -> UIBezierPath {
        
        struct PathCalc {
            
            private let rect: CGRect
            
            init(withRect rect: CGRect) {
                self.rect = rect
            }
            
            var topEdgeCenter: CGPoint {
                return CGPoint(
                    x: rect.midX,
                    y: rect.minY
                )
            }
            
            var rightEdgeCenter: CGPoint {
                return CGPoint(
                    x: rect.maxX,
                    y: rect.midY
                )
            }
            
            var bottomEdgeCenter: CGPoint {
                return CGPoint(
                    x: rect.midX,
                    y: rect.maxY
                )
            }
            
            var leftEdgeCenter: CGPoint {
                return CGPoint(
                    x: rect.minX,
                    y: rect.midY
                )
            }
        }
        
        let calc = PathCalc(withRect: rect)
        let path = UIBezierPath()
        
        // Start at top center, then draw lines to the middle of each other side.
        path.move(to: calc.topEdgeCenter)
        path.addLine(to: calc.rightEdgeCenter)
        path.addLine(to: calc.bottomEdgeCenter)
        path.addLine(to: calc.leftEdgeCenter)
        
        // We're done!
        path.close()
        return path
        
    }
    
    // Get the path for a squiggle.
    private static func getSquiggleShapePath(inRect rect: CGRect) -> UIBezierPath {
        
        struct PathCalc {
            
            private let rect: CGRect
            
            static let BluntTurn: CGFloat = 10.0
            static let SharpTurnSmooth: CGFloat = 16.0
            static let SharpTurnKick: CGFloat = 4.0
            
            init(withRect rect: CGRect) {
                self.rect = rect
            }
            
            var leftEdgeCenter: CGPoint {
                return CGPoint(
                    x: rect.minX,
                    y: rect.midY
                )
            }
            
            var topLeftTurnControlPoint1: CGPoint {
                return CGPoint(
                    x: rect.minX,
                    y: rect.minY + rect.height / PathCalc.BluntTurn
                )
            }
            
            var topLeftTurnControlPoint2: CGPoint {
                return CGPoint(
                    x: rect.minX + rect.width / PathCalc.BluntTurn,
                    y: rect.minY
                )
            }
            
            var topCenterInflectionPoint: CGPoint {
                return CGPoint(
                    x: rect.midX,
                    y: rect.minY + rect.height / PathCalc.SharpTurnKick
                )
            }
            
            var topRightTurnControlPoint1: CGPoint {
                return CGPoint(
                    x: rect.maxX - rect.width / PathCalc.SharpTurnSmooth,
                    y: rect.midY
                )
            }
            
            var topRightTurnControlPoint2: CGPoint {
                return CGPoint(
                    x: rect.maxX + rect.width / PathCalc.SharpTurnSmooth,
                    y: rect.minY - rect.height / PathCalc.SharpTurnKick
                )
            }
            
            var rightEdgeCenter: CGPoint {
                return CGPoint(
                    x: rect.maxX,
                    y: rect.midY
                )
            }
            
            var bottomRightTurnControlPoint1: CGPoint {
                return CGPoint(
                    x: rect.maxX,
                    y: rect.maxY - rect.height / PathCalc.BluntTurn
                )
            }
            
            var bottomRightTurnControlPoint2: CGPoint {
                return CGPoint(
                    x: rect.maxX - rect.width / PathCalc.BluntTurn,
                    y: rect.maxY
                )
            }
            
            var bottomCenterInflectionPoint: CGPoint {
                return CGPoint(
                    x: rect.midX,
                    y: rect.maxY - rect.height / PathCalc.SharpTurnKick
                )
            }
            
            var bottomLeftTurnControlPoint1: CGPoint {
                return CGPoint(
                    x: rect.minX + rect.width / PathCalc.SharpTurnSmooth,
                    y: rect.midY
                )
            }
            
            var bottomLeftTurnControlPoint2: CGPoint {
                return CGPoint(
                    x: rect.minX - rect.width / PathCalc.SharpTurnSmooth,
                    y: rect.maxY + rect.height / PathCalc.SharpTurnKick
                )
            }
            
        }
        
        let calc = PathCalc(withRect: rect)
        
        let path = UIBezierPath()
        
        path.move(to: calc.leftEdgeCenter)
        path.addCurve(to: calc.topCenterInflectionPoint,
                      controlPoint1: calc.topLeftTurnControlPoint1,
                      controlPoint2: calc.topLeftTurnControlPoint2)
        path.addCurve(to: calc.rightEdgeCenter,
                      controlPoint1: calc.topRightTurnControlPoint1,
                      controlPoint2: calc.topRightTurnControlPoint2)
        path.addCurve(to: calc.bottomCenterInflectionPoint,
                      controlPoint1: calc.bottomRightTurnControlPoint1,
                      controlPoint2: calc.bottomRightTurnControlPoint2)
        path.addCurve(to: calc.leftEdgeCenter,
                      controlPoint1: calc.bottomLeftTurnControlPoint1,
                      controlPoint2: calc.bottomLeftTurnControlPoint2)
        
        return path
        
        
    }
    
    // Get the path for an oval.
    private static func getOvalShapePath(inRect rect: CGRect) -> UIBezierPath {
        struct PathCalc {
            
            private let rect: CGRect
            
            init(withRect rect: CGRect) {
                self.rect = rect
            }
            
            var turnRadius: CGFloat {
                return rect.height / 2
            }
            
            var rightCircleCenter: CGPoint {
                return CGPoint(
                    x: rect.maxX - turnRadius,
                    y: rect.midY
                )
            }
            
            var leftCircleCenter: CGPoint {
                return CGPoint(
                    x: rect.minX + turnRadius,
                    y: rect.midY
                )
            }
        }
        
        let calc = PathCalc(withRect: rect)
        
        let path = UIBezierPath()
        path.addArc(withCenter: calc.rightCircleCenter,
                    radius: calc.turnRadius,
                    startAngle: -0.5 * CGFloat.pi,
                    endAngle: 0.5 * CGFloat.pi,
                    clockwise: true
        )
        path.addArc(withCenter: calc.leftCircleCenter,
                    radius: calc.turnRadius,
                    startAngle: 0.5 * CGFloat.pi,
                    endAngle: 1.5 * CGFloat.pi,
                    clockwise: true)
        path.close()
        
        return path
        
    }
    
    // Get the appropriate shape path for this card.
    private func getShapePath(inRect rect: CGRect) -> UIBezierPath {
        
        switch (self.shape) {
            case .Diamond:
                return CardView.getDiamondShapePath(inRect: rect)
            case .Squiggle:
                return CardView.getSquiggleShapePath(inRect: rect)
            case .Oval:
                return CardView.getOvalShapePath(inRect: rect)
        }
    }
    
    // Pick color to use.
    private static func translateToUIColor(fromEnum color: Color) -> UIColor {
        switch color {
            case .Green:
                return UIColor.green
            case .Purple:
                return UIColor.purple
            case .Red:
                return UIColor.red
        }
    }
    
    // Apply coloring to the path given.
    private func applyColoring(toPath shapePath: UIBezierPath) {
        
        struct Params {
            // Thickness of the shape edge
            static let EdgeLineThickness: CGFloat = 3.0
            
            // Thickness of a stripe line
            static let StripeLineThickness: CGFloat = 2.0
            
            // Distance between stripe lines
            static let DistanceBetweenStripeLines: CGFloat = 5.0
            
        }
        
        // Pick the right color.
        let uiColor = CardView.translateToUIColor(fromEnum: self.color)
        
        // Apply the color as per the drawing mode.
        switch (self.shading) {
            
            // If "striped", we draw lots of fun little thin vertical lines.
            case .Striped:
                
                let linePath = UIBezierPath()
                linePath.lineWidth = Params.StripeLineThickness
                
                for x in stride(from: shapePath.bounds.minX, to: shapePath.bounds.maxX, by: Params.DistanceBetweenStripeLines) {
                    linePath.move(to: CGPoint(x: x, y: shapePath.bounds.minY))
                    linePath.addLine(to: CGPoint(x: x, y: shapePath.bounds.maxY))
                }
                
                let context = UIGraphicsGetCurrentContext()
                
                context!.saveGState()
                
                // Draw the stripes, clip with the shape.
                shapePath.addClip()
                uiColor.setStroke()
                linePath.stroke()
                
                context!.restoreGState()
                
                // Also run the "open" case below to add a border around the shape!
                fallthrough
            
            // If "open", we just stroke but don't fill.
            case .Open:
                shapePath.lineWidth = Params.EdgeLineThickness
                uiColor.setStroke()
                shapePath.stroke()
            
            // If solid, fill with the color.
            case .Solid:
                uiColor.setFill()
                shapePath.fill()
        }
        
        
    }
    
    // Add the shape in a UILabel fitting the given rectangle.
    // The UILabel will be added to the subview as part of this.
    private func drawShape(inRect rect: CGRect) {
        
        // Get the appropriate shape, and apply the proper coloring on it.
        let shapePath = getShapePath(inRect: rect)
        applyColoring(toPath: shapePath)
    }
    
    // Draw the card
    override func draw(_ rect: CGRect) {
        
        struct Params {
            // Thickness of the card label
            static let CardEdgeThickness: CGFloat = 3.0
            
            // Alpha for unlighted state
            static let UnhighlightAlpha: CGFloat = 0.15
            
            // Alpha for highlighting
            static let HighlightAlpha: CGFloat = 0.5
        }
        
        // Clear current graphics, to avoid side effects from a fresh drawing.
        UIGraphicsGetCurrentContext()!.clear(rect)
        
        // Rounded edge on the cards
        let roundedCardEdge = UIBezierPath(roundedRect: bounds, cornerRadius: cardCornerRadius)
        roundedCardEdge.addClip()
        roundedCardEdge.lineWidth = Params.CardEdgeThickness
        
        // Always stroke the edge with gray.
        UIColor.gray.setStroke()
        roundedCardEdge.stroke()
        
        // Fill the card with the appropriate color depending on selection state.
        switch self.selectionState {
            case .Unselected:
                UIColor.gray.withAlphaComponent(Params.UnhighlightAlpha).setFill()
            case .Selected:
                UIColor.blue.withAlphaComponent(Params.HighlightAlpha).setFill()
            case .MatchFailed:
                UIColor.red.withAlphaComponent(Params.HighlightAlpha).setFill()
            case .MatchSucceeded:
                UIColor.green.withAlphaComponent(Params.HighlightAlpha).setFill()
        }
        roundedCardEdge.fill()
        
        // Draw either the contents, or nothing, depending on whether the
        // card is face up or down.
        switch self.faceState {
            
            case .Down:
                // Do nothing.
                break
            
            case .Up:
                
                // Get the shape rectangles within the one provided.
                let shapeRects = self.getShapeRects()
                
                // Create a fully formed UILabel in each rectangle.
                for shapeRect in shapeRects {
                    self.drawShape(inRect: shapeRect)
            }
        }
    }
    
}

extension CardView {
    
    private struct BoundsSizeRatio {
        
        // How high each shape label should be, proportional to bounds height
        static let shapeLabelHeight: CGFloat = 0.25
        
        // How wide each shape label should be, proportional to bounds width
        static let shapeLabelWidth: CGFloat = 0.8
        
        // How much padding there should be between shape labels proportional to height
        static let shapeLabelVerticalPadding: CGFloat = 0.05
        
        // What should the corner radius on the card be to make it look round, proportional to card height
        static let cardCornerRadius: CGFloat = 0.06
    }
    
    // Return how high a shape label should be.
    private var shapeLabelHeight: CGFloat {
        return bounds.size.height * BoundsSizeRatio.shapeLabelHeight
    }
    
    // Return how wide a shape label should be.
    private var shapeLabelWidth: CGFloat {
        return bounds.size.width * BoundsSizeRatio.shapeLabelWidth
    }
    
    // Return how much padding there should be between shapes.
    private var shapeLabelVerticalPadding: CGFloat {
        return bounds.size.height * BoundsSizeRatio.shapeLabelVerticalPadding
    }
    
    // Return how round the card corners should be
    private var cardCornerRadius: CGFloat {
        return bounds.size.height * BoundsSizeRatio.cardCornerRadius
    }
    
    // Get top left point for origin.
    // Calculated by subtracting half the desired width from the center X,
    // and subtracting half the desired height from the center Y.
    private func getTopLeftPoint(forCenter center: CGPoint) -> CGPoint {
        return CGPoint(
            x: center.x - shapeLabelWidth / 2,
            y: center.y - shapeLabelHeight / 2
        )
        
    }
    
    // Move a point up by only padding amount.
    private func shiftUpByPadding(fromPoint point: CGPoint) -> CGPoint {
        return CGPoint(
            x: point.x,
            y: point.y - ((shapeLabelVerticalPadding / 2) + (shapeLabelHeight / 2))
        )
    }
    
    // Move a point up by padding amount and card height.
    private func shiftUpByPaddingAndCardHeight(fromPoint point: CGPoint) -> CGPoint {
        return CGPoint(
            x: point.x,
            y: point.y - (shapeLabelVerticalPadding + shapeLabelHeight)
        )
    }
    
    // Move a point down by only padding amount.
    private func shiftDownByPadding(fromPoint point: CGPoint) -> CGPoint {
        return CGPoint(
            x: point.x,
            y: point.y + ((shapeLabelVerticalPadding / 2) + (shapeLabelHeight / 2))
        )
    }
    
    // Move a point down by padding amount and card height.
    private func shiftDownByPaddingAndCardHeight(fromPoint point: CGPoint) -> CGPoint {
        return CGPoint(
            x: point.x,
            y: point.y + (shapeLabelVerticalPadding + shapeLabelHeight)
        )
    }
    
    // Get rectangles for the appropriate number of labels, positioned correctly.
    private func getShapeRects() -> [CGRect] {
        
        var rects: [CGRect] = []
        
        // Top left bounds center, all positioning will be relative to this
        let topLeftFromBoundsCenter = getTopLeftPoint(forCenter: CGPoint(x: bounds.midX, y: bounds.midY))
        
        // Dimensions of each rectangle will be identical
        let rectSize = CGSize(width: shapeLabelWidth, height: shapeLabelHeight)
        
        switch (self.number) {
            
            // Just one rectangle in the center of the bounds
            case .One:
                
                // The one and only
                rects.append(CGRect(
                    origin: topLeftFromBoundsCenter,
                    size: rectSize)
                )
            
            // Padding in the center, two rectangles above and below the padding
            case .Two:
            
                // Top rectangle
                rects.append(CGRect(
                    origin: shiftUpByPadding(fromPoint: topLeftFromBoundsCenter),
                    size: rectSize)
                )
            
                // Bottom rectangle
                rects.append(CGRect(
                    origin: shiftDownByPadding(fromPoint: topLeftFromBoundsCenter),
                    size: rectSize)
                )
            
            case .Three:
            
                // Top rectangle
                rects.append(CGRect(
                    origin: shiftUpByPaddingAndCardHeight(fromPoint: topLeftFromBoundsCenter),
                    size: rectSize)
                )
            
                // Middle rectangle
                rects.append(CGRect(
                    origin: topLeftFromBoundsCenter,
                    size: rectSize)
                )
            
                // Bottom rectangle
                rects.append(CGRect(
                    origin: shiftDownByPaddingAndCardHeight(fromPoint: topLeftFromBoundsCenter),
                    size: rectSize)
                )
        }
        
        // Return the result of the rectangle positioning calculations.
        return rects
        
    }
    
    
}
