//
//  Array.swift
//  SetGame
//
//  Created by Sauhard Sahi on 12/8/18.
//  Copyright © 2018 Sauhard Sahi. All rights reserved.
//

import Foundation

extension Array {
    
    // Tells whether the given index is between 0 and count.
    func isInRange(index: Int) -> Bool {
        return index >= 0 && index < self.count
    }
    
    // Moves each element from its old index to a new one, per the
    // transform mapping provided. Assumed that the number of indices provided
    // matches the number of elements in this array, no safety checks are performed.
    // (Modifies current array)
    mutating func rearrange(old2NewIndices indexRemap: [Int]) {
        self = rearranged(old2NewIndices: indexRemap)
    }
    
    // Moves each element from its old index to a new one, per the
    // transform mapping provided. Assumed that the number of indices provided
    // matches the number of elements in this array, no safety checks are performed.
    // (Returns new array)
    func rearranged(old2NewIndices indexRemap: [Int]) -> [Element] {
        return Array<Int>(0..<self.count).map( { self[indexRemap.firstIndex(of: $0)!] })
    }
}
